<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Picture;
use App\Form\UploadPictureType;

class PictureController extends AbstractController
{
    /**
     * @Route("/admin/pictures", name="admin_pictures")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR', null, 'Impossible d\'accèder à cette page!');
        
        // Sélection de toutes les photos
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();
        // Pagination
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, 
            $page === 0 ? 1: $page, 
            12
        );
        return $this->render('admin/pictures/index.html.twig', [
            'pictures' => $pictures,
        ]);
      
    }

    /**
     * @Route("/admin/pictures/edit/{id}", name="admin_pictures_edit")
     */
    public function edit($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR', null, 'Impossible d\'accèder à cette page!');
        // Sélectionne un enregistrement en BDD
        $picture= $this->getDoctrine()->getRepository(Picture::class)->find($id);
        if(!$picture){
            throw $this->createNotFoundException('Cette photo n\'existe pas');
        }
        $formEdit = $this->createForm(UploadPictureType::class,$picture,[
            'validation_groups'=> 'edit_picture'
        ]);
        // Hydratation des données
        $formEdit->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
            // Ajoute la date du jour de l'update
            $picture->setUpdatedAt(new \DateTimeImmutable());
            

            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            
            $doctrine->persist($picture);
           
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Merci pour la mise à jour !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin_pictures');
        }
    return $this->render('admin/pictures/edit.html.twig', [
        'formEdit' => $formEdit->createView()
    ]);
    
    }
    /**
     * @Route("/admin/pictures/delete/{id}", name="admin_pictures_delete")
     */
    public function delete($id)
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR', null, 'Impossible d\'accèder à cette page!');
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        if(!$picture){
            throw $this->createNotFoundException('Cette photo n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo a bien été supprimée');

        return $this->redirectToRoute('admin_pictures');
        
    }

}

