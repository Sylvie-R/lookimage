<?php

namespace App\Controller\Admin;

use app\Form\EditUserType;
use App\Entity\User;
use App\Repository\UserRepository;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/user", name="admin_user")
     */
    public function index(UserRepository $users,PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');

        // Sélection de tous les utilisateurs
        $query = $this->getDoctrine()->getRepository(user::class)->findAll();
        
        // Pagination
        $page = $request->query->getInt('page', 1);
      
        $users = $paginator->paginate(
            $query, 
            $page === 0 ? 1: $page, 
            12
        );
        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }
    /**
     * @Route("/admin/user/edit/{id}", name="admin_user_edit")
     */
    public function userEdit(User $user, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');

        // Sélectionne un enregistrement en BDD
        $user= $this->getDoctrine()->getRepository(User::class)->find($user);
        if(!$user){
            throw $this->createNotFoundException('Cette photo n\'existe pas');
        }
        $formEdit = $this->createForm(EditUserType::class, $user);

        // Hydratation des données
        $formEdit->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
                
            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            
            $doctrine->persist($user);
           
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Utilisateur mis à jour !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin_user');
        }
        return $this->render('admin/user/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]);
    }
    /**
     * @Route("/admin/user/delete/{id}", name="admin_user_delete")
     */
    public function delete($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');
        
        $user= $this->getDoctrine()->getRepository(User::class)->find($id);
        if(!$user){
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }
        
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success', 'Cet utilisateur a bien été supprimé');

        return $this->redirectToRoute('admin_user');
        
    }
        
}
