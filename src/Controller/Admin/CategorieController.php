<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use Symfony\Component\String\Slugger\AsciiSlugger;
use App\Form\CreateNewCategoryType;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use App\Repository\PictureRepository;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/admin/categorie", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');

        // Sélection de toutes les categories
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        
        // Sélection de toutes les photos
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();
        // Pagination
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, 
            $page === 0 ? 1: $page, 
            12
        );
        return $this->render('admin/index.html.twig', [
            'categories' => $categories,
            'pictures' => $pictures
        ]);
    }
    /**
     * @Route("/admin/categorie/new", name="admin_categorie_new")
     */
    public function new(Request $request)
    {
       $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');
        
        // Instancie l'entité categorie
        $category = new Categorie();
        // Formulaire
        $formNewCat = $this->createForm(CreateNewCategoryType::class, $category);
        // Hydratation des données
        $formNewCat->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formNewCat->isSubmitted() && $formNewCat->isValid()){
            // Ajout du slug pris dans dataFixture
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            // récupère les données stocker dans $picture
            $doctrine->persist($category);
            //commande insertion
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Nouvelle catégorie créée');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/categories/new.html.twig', [
            'formNewCat' => $formNewCat->createView()
        ]);
    }
    /**
     * @Route("/admin/categorie/edit/{id}", name="admin_categorie_edit")
     */
    public function edit($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');
        // $hasAccess = $this->isGranted('ROLE_ADMIN');
        // Sélectionne un enregistrement en BDD
        $category= $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        if(!$category){
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }
        // Création du formulaire
        $formEdit = $this->createForm(CreateNewCategoryType::class, $category);
        // Hydratation des données
        $formEdit->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
            // Ajout du slug pris dans dataFixture
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));
            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            
            $doctrine->persist($category);
           
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Mise à jour effectuée avec succès!');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/categories/edit_cat.html.twig', [
        'formEdit' => $formEdit->createView()
        ]);
    }
    /**
     * @Route("/admin/categorie/delete/{id}", name="admin_categorie_delete")
     */
    public function delete($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');
        
        $categorie= $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        if(!$categorie){
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }
        
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($categorie);
        $doctrine->flush();

        $this->addFlash('success', 'La catégorie a bien été supprimée');

        return $this->redirectToRoute('admin');
        
    }
       
}
