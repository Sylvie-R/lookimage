<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use App\Repository\PictureRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        // getDoctrine()= connexion de la bdd - getRepository() = de l'entité Picture - 
        // findAll() retourne moi tout
        // $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        // Equivalent : SELECT * FROM picture WHERE is_valideted = true
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy(['is_validated'=> true]);

        // Création du page par page
        // paginate() attend 3 arguments
            // 1. la collection contenatn tous les resultats ici toutes les images
            // 2. la page sur laquelle nous sommes, page 1 par défaut, envoyé par url getInt()
            // 3.le nombred 'élements par page
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, 
            $page === 0 ? 1: $page, 
            12
        );

        // Sélection de toutes le categories
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        
        
        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
        ]);
         
    }
    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, PaginatorInterface $paginator, Request $request)
    {
        // ddveut dire "Dump Data", arrête tout le chargement dès que le programme tombe dessus et affiche son contenu
        // équivalent de var dump(), lui laisse la page se charger complètement et affiche le contenu dans le debug bar
        // dd($id);

        // find() Sélectionne UN de la catégorie par son id
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category'=> $category,
            'is_validated'=> true
            ]);
           

          if(!$category){
              //  Si aucune photo n'est trouvé, nous créons une execption
              throw $this->createNotFoundException('Aucune photo dans cette catégorie');
          }

        // Création du page par page pour les images liées à la catégorie
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            // $category->getPictures(), 
            $query,
            $page === 0 ? 1: $page, 
            2
        );
          // Si des photos existent nous envoyons les données à la vue
          return $this->render('home/picturesByCategory.html.twig',  [
            // 'pictures' => $pictures,
            'category' => $category,
            'pictures' => $pictures
            ]
        );
    }

    /**
     * requirements permet de valider le type de la donnée passé en paramètre
     * Afficher l'image cliquée
     * @Route("/picture/{id}", name="picture_view", requirements={"id"="\d+"})
     * 
     */
    public function pictureView($id, PaginatorInterface $paginator, Request $request)
    {
        // dd($id);
         // find() Sélectionne UN de la catégorie par son id
         $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
         $pictures = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category'=> $category,
            'is_validated'=> true
            ]);
        
         if(!$picture){
            //  Si aucune photo n'est trouvé, nous créons une execption
            throw $this->createNotFoundException('Cette photo n\'existe pas');
        }
        // Création du page par page pour les images liées à la catégorie de l'image
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
        // $picture->getCategory()->getPictures(), 
        $pictures,
        $page === 0 ? 1: $page, 
            4
        );
        // Si des photos existent nous envoyons les données à la vue
        return $this->render('home/pictureView.html.twig',  [
            'picture' => $picture,
            'photos' => $photos
             
        ]);
    }
}
