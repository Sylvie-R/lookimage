<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        // Autre méthode pour sécurité l'accès
        // $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Impossible d\'accèder à cette page!');

        /**
         * $this->getUser() est l'équivalent de app.user en twig
         * par exemple, si je veux le prenom : $this->getUser()->getFirstname()
         */
        $page = $request->query->getInt('page', 1);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            // on travaille avec les entités
            'user' => $this->getUser()
        ]);

        $pictures = $paginator->paginate(
            $query,
            $page === 0 ?1 : $page,
            20
        );
             
        return $this->render('account/index.html.twig', [
           'pictures' =>$pictures 
        ]);
    }
    /**
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request)
    {
        // Instancie l'entité "picture"
        $picture = new Picture();

        //Premier paramètre :le formulaire dont on a beosin
        //Deuxième paramètre l'objet de l'entité à vide
        $formUpload = $this->createForm(UploadPictureType::class,$picture,[
            'validation_groups'=> 'new_picture'
        ]);
        // Hydratation des données
        $formUpload->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formUpload->isSubmitted() && $formUpload->isValid()){
            // Ajoute la date du jour
            $picture->setCreatedAt(new \DateTimeImmutable());
            // Ajoute l'utilisateur connecté à notre setter
            $picture->setUser($this->getUser());
            $picture->setIsValidated(false);

            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            // récupère les données stocker dans $picture
            $doctrine->persist($picture);
            //commande insertion
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Merci pour votre partage !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }

    /**
     * Affiche les erreurs d'un formulaire grâce à la méthode getErrors()
     * <div class="alert alert-danger">
     *      {% for error in errors %}
     *         <p>{{ error.message }}</p>
     *    {% endfor %}
     *   </div>
     *  */ 
        
    #}

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView()
        ]);
    }
    /**
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request)
    {
        
        // Sélectionne un enregistrement en BDD
        $picture= $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Sécurité
        if(!$picture || $this->getUser() !== $picture->getUser()){
            //  Si aucune photo n'est trouvé, retourne erreur 404
            // ou si l'image n'appartient pas à l'utilisateur connecté
            throw $this->createNotFoundException('Cette photo n\'existe pas');

        // if ($this->getUser() !== $picture->getUser()) {
            // throw $this->createAccessDeniedException('La photo n\'existe pas.'); erreur403 --- accès refusé

        }
        //Premier paramètre :le formulaire dont on a beosin
        //Deuxième paramètre l'objet de l'entité à vide
        $formEdit = $this->createForm(UploadPictureType::class,$picture,[
            'validation_groups'=> 'edit_picture'
        ]);
        // Hydratation des données
        $formEdit->handleRequest($request);

        // Vérifie si formulaire envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
            // Ajoute la date du jour de l'update
            $picture->setUpdatedAt(new \DateTimeImmutable());
            

            // Insertion en BDD - doctrine protège les données
            $doctrine = $this->getDoctrine()->getManager();
            
            $doctrine->persist($picture);
           
            $doctrine->flush();

            // Message flash indique si données bien inséré
            $this->addFlash('success', 'Merci pour la mise à jour !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }
    return $this->render('account/edit.html.twig', [
        'formEdit' => $formEdit->createView()
    ]);
    
    }
    /**
     * @Route("/account/delete/picture/{id}", name="account_delete_picture")
     */
    public function delete($id)
    {
        
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Sécurité
        if(!$picture || $this->getUser() !== $picture->getUser()){
            throw $this->createNotFoundException('Cette photo n\'existe pas');

        }
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo a bien été supprimée');

        return $this->redirectToRoute('account');
        
    }

}
