<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;
    /**
     * Récupération du système de chiffrement du mot de passe
     */

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $faker= Faker\Factory::create('fr_FR');

        for($i=0; $i<=10; $i++){
            $user= new User();
            $user->setEmail($faker->email);
            $user->SetFirstname($faker->firstName);
            $user->setLastname($faker->lastName);
            // chiffrage du mot de passe via encoder - attend l'utilisateur et mot de passe
            $user->setPassword($this->encoder->encodePassword($user, 'secret'));
            $user->setIsVerified(true);

            // Enregistre chaque utilisateur dans une référence 
            $this->addReference('user_'.$i,$user);
            // Persistance en bdd
            $manager->persist($user);

        }

        $manager->flush();
    }
}
