<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
// insère les lignes suivantes
use App\Entity\Picture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker;
use Mmo\Faker\PicsumProvider;
use Symfony\Component\HttpFoundation\File\File;

// On fait une implementation de classe pour lui dire dans quel ordre faire les ifxtures
class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        /**
         * Premet de dire à notre fixtures, si elle depend d'autres fixtures 
         * afin de ne pas avoir d'erreurs lors de l'utilisations des "getReference()"
         */

         return [
             CategorieFixtures::class,
             UserFixtures::class
         ];
    }
    public function load(ObjectManager $manager)

    {
        // Instancie Faker avec des données en français
        $faker = Faker\Factory::create('fr_FR');

        // On ajout de bundle "Mmo Faker-image" à Faker
        $faker->addProvider(new PicsumProvider($faker));

        // Création d'une boucle for pour choisir le nbre d'elements allant en BDD
        for($i = 0; $i <=50; $i++){

            // Spécifier le chemein du dossier d'upload
            // Les deux autres paramètres sont la hauteur et la largeur de l'image à récupérer
            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152,2312), random_int(864,1736));

            // Récupération d'une référence aléatoirement
            //  On récupère un objet de l'entité Categorie généré dans le fichier CategorieFixtures grâce au nom choisi lors de l'enregistrement dans les références lié à $i et pas id
            $category=$this->getReference('category_'.random_int(0,10));

            // Récupère une référence User aléatoirement
            $user = $this->getReference('user_'.random_int(0,10));

            // On instancie la table dans laquelle on veut
            $picture = new Picture();
            // selon la doc faker
            $picture->setDescription($faker->sentence(26));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('- 4 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('- 2 years'));
            $picture->setUser($user);

            // relation de table ici il attend setCategory(?Categorie $category)
            $picture->setCategory($category);

            // Gestion de l'image attend un type file
            // A insérer en haut use Symfony
            $picture->setImageFile(new File($image));

            // str_peplace() permet de chercher un morceau de caractères dans un e chaine de caractère et nous pouvons le remplacer par ce qu'on souhaite, dans notre cas par rien
            // 1argument: chaine à rechercher
            // 2 argument par quoi remplacer le 1er argument
            // 3 argument  où effectuer la recherche
            $picture->setImage(str_replace('./public/uploads/images/photos\\','', $image));

            // Garde de côté en attendant l'execution des requêtes
            $manager->persist($picture);
        }
        $manager->flush();
    }

}
