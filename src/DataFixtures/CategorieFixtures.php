<?php

namespace App\DataFixtures;
// Vérifie que cette ligne a été crée
use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
// insère les lignes suivantes
use Symfony\Component\String\Slugger\AsciiSlugger;
use Faker;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Instancie Faker avec des données en français
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for pour choisir le nbre d'elements allant en BDD
        for($i = 0; $i <=10; $i++){
            // On instancie la table dans laquelle on veut
            $category = new Categorie();
            // selon la doc faker
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

            // Enregistre l'objet dans une référence, càd que l'on pourra utiliser les objets enregistrés dans une autre fixture afin de pouvoir effectuer des relations de table
            // addReference() attend 2 arguments : 1 nom unique, et 1 objet lié a ce nom
            $this->addReference('category_'.$i, $category);

            // Garde de côté en attendant l'execution des requêtes
            $manager->persist($category);

        }
        
        $manager->flush();
    }
}
