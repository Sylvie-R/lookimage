<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,[
                'required' =>true,
                'label' => 'Adresse email',
                'constraints'=>[
                    new NotBlank([
                    'message' => 'Merci de saisir une adresse email'
                ])
                    ]
            ])
            ->add('roles', ChoiceType::class,[
                'required' =>true,
                'label' => 'Rôles',
                'choices' => [
                    'Utilisateur' =>'ROLE_USER',
                    'Administrateur' =>'ROLE_ADMIN',
                    'Editeur'=>'ROLE_EDITOR'
                ],
                //Case à cocher
                'expanded'=> true,
                'multiple'=> true,
            ])
            ->add('Valider', SubmitType::class)
            ->add('firstname', TextType::class,[
                'required'=>true,
                'label'=>'Prénom',
                'constraints'=>[
                    new NotBlank([
                    'message' => 'Merci de saisir une adresse email'
                ])
                ]
            ])
            ->add('lastname', TextType::class,[
                'required'=>true,
                'label'=>'Nom',
                'constraints'=>[
                    new NotBlank([
                    'message' => 'Merci de saisir une adresse email'
                ])
                ]
            ])
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
