<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class RegistrationFormType extends AbstractType
{
    // Champs du formulaire
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        // Dans add 3 paramètres: nom, type et 1 tableau
            ->add('firstname', TextType::class, [
                'required' => true, // Rends le champs obligatoire en ajoutant l'attribut "required"
                'label' => 'Prénom', // Choix du nom du label, si on n'en veut pas mettre=>false
                // si on veut un placeholer
                /**
                 * 'attr'=>[
                 * 'placeholder' => 'Veuillez saisir un prénom',
                 * 'class' => 'nameClassCSS],
                 */
                'constraints' =>[ // contraintes de validation / côté serveur
                    // permet de vérifier  que le champ n'est pas vide
                    new NotBlank([
                        'message'=>'Veuillez saisir un prénom'
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Nom',
                'constraints' =>[
                    new NotBlank([
                        'message'=>'Veuillez saisir un nom'
                    ])
                ]
            ])
            ->add('email', EmailType::class,[
                'required' => true,
                'label' => 'Adresse mail',
                'constraints' =>[
                    new NotBlank([
                        'message'=>'Veuillez saisir une adresse mail'
                    ]),
                    new Email([
                        'message'=>'Votre email est invalide'
                    ])
                
                ]
            ])
            // mapped spécifie qu'il n'existe pas dans l'entité User - pas d'insertion en bd
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false, 
                'label'=> '« En soumettant ce formulaire, j’accepte que MONSITE conserve mes données personnelles via ce formulaire. Aucune exploitation
                commerciale ne sera faite des données conservées.»',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez acceptez les RGPD..',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas.',
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Répeter votre mot de passe'],
                
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir minimum {{ limit }} caractères',
                        'max' => 4096,
                    ]),
                ],
            ])
            
        ;
    }
// Ce formulaire est relié à l'entité User
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
